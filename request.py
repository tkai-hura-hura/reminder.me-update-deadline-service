#!/usr/bin/python
import json

def validated_body(request):
    if 'body' in request and request['body'] != None:
        try:
            payload = json.loads(request['body'])
        except:
            payload = request['body']
        
        return payload
    else:
        raise Exception("Invalid request")

def validated_field(payload, field):
    return payload[field] if field in payload else None