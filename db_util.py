#!/usr/bin/python
import os
import psycopg2

from dotenv import load_dotenv

load_dotenv()

db_host = os.getenv('DB_HOST')
db_port = os.getenv('DB_PORT')
db_name = os.getenv('DB_NAME')
db_user = os.getenv('DB_USER')
db_pass = os.getenv('DB_PASS')

def make_conn():
    conn = None
    try:
        conn = psycopg2.connect("dbname='%s' user='%s' host='%s' password='%s'" % (
            db_name, db_user, db_host, db_pass))
    except:
        print("I am unable to connect to the database")
        raise Exception("Connection failed")
    return conn

def fetch_data(conn, query):
    result = []
    print("Now executing: ", query)
    cursor = conn.cursor()
    
    try:
        cursor.execute(query)
    except:
        raise Exception("Wrong data")

    raw = cursor.fetchall()
    for line in raw:
        data = {}
        c = 0
        for col in cursor.description:
            data.update({str(col[0]): line[c]})
            c = c+1

        result.append(data)

    return result

def change_data(conn, query):
    print("Now executing: ", query)
    cursor = conn.cursor()
    
    try:
        cursor.execute(query)
    except:
        raise Exception("Wrong data")
    conn.commit()