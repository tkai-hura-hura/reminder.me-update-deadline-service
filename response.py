#!/usr/bin/python
import json

def http_response(success, status_code, status_msg, message, data):
    return {
        "statusCode": status_code,
        "statusDescription": status_msg,
		"isBase64Encoded": False,
        "headers": {
            "Content-Type": "application/json"
        },
        "body": json.dumps({
            "success": success,
            "status": status_code,
            "message": message,
            "data": json.loads(json.dumps(data, default=str))
        })
    }

def http_200(data):
    return http_response(True, 200, "200 OK", "Succeeded", data)

def http_400():
    return http_response(False, 400, "400 Bad Request", "Request body is invalid", None)

def http_404():
    return http_response(False, 404, "404 Not Found", "Data not found", None)

def http_500():
    return http_response(False, 500, "500 Internal Server Error", "Internal server error", None)