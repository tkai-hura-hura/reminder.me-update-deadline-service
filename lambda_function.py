#!/usr/bin/python
import re
import sys
import logging
import psycopg2

from db_util import make_conn, fetch_data, change_data
from request import validated_body, validated_field
from response import http_200, http_400, http_404, http_500

def lambda_handler(event, context):
 
    print("Requests: ", event)
    try:
        payload = validated_body(event)
    except:
        return http_400()

    id = validated_field(payload, 'id')
    name = validated_field(payload, 'name')
    desc = validated_field(payload, 'description')
    matkul = validated_field(payload, 'matkul')
    due = validated_field(payload, 'due')
    
    try:
        conn = make_conn()
    except:
        return http_500()

    try:
        update_deadline_by_id(conn, id, name=name, desc=desc, matkul=matkul, due=due)
        data = get_deadline_by_id(conn, id)
    except:
        return http_404()

    conn.close()

    return http_200(data[0])

def update_deadline_by_id(conn, id, name=None, desc=None, matkul=None, due=None):
    query_update = "UPDATE \"reminder-me\".deadline SET "
    
    if name != None:
        query_update += "name='{name}', ".format(name=name)
    if desc != None:
        query_update += "description='{desc}', ".format(desc=desc)
    if matkul != None:
        query_update += "matkul='{matkul}', ".format(matkul=matkul)
    if due != None:
        query_update += "due_datetime='{due}', ".format(due=due)
    
    query_update = re.sub('\, $', ' ', query_update)
    query_update += "WHERE id={id}".format(id=id)
    
    change_data(conn, query_update)
    
def get_deadline_by_id(conn, id):
    query_select = "SELECT * FROM \"reminder-me\".deadline WHERE id={id};".format(id=id)
    return fetch_data(conn, query_select)